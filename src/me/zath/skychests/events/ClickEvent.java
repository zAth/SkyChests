package me.zath.skychests.events;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skychests.Config;
import me.zath.skychests.SkyChests;
import me.zath.skychests.controllers.ChestController;
import me.zath.skychests.gui.ConfirmGui;
import me.zath.skychests.gui.IconGui;
import me.zath.skychests.gui.MainGui;
import me.zath.skychests.gui.utils.GuiHolder;
import me.zath.skychests.gui.utils.GuiItems;
import me.zath.skychests.objects.Chest;
import me.zath.skychests.utils.SQL;
import me.zath.skychests.utils.Utils;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

public class ClickEvent implements Listener{

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        if ((e.getInventory().getHolder() instanceof GuiHolder)) {
            Player p = (Player) e.getWhoClicked();
            if (e.getCurrentItem() == null) {
                e.setCancelled(true);
                return;
            }
            if (e.getRawSlot() < e.getInventory().getSize()) {
                ItemStack itemStack = e.getCurrentItem();
                String dono = ((GuiHolder) e.getInventory().getHolder()).getDono();
                int id = ((GuiHolder) e.getInventory().getHolder()).getId();
                Chest chest = ChestController.get(dono, id);

                switch (((GuiHolder) e.getInventory().getHolder()).getType()){
                    case CHEST:
                        if(!dono.equalsIgnoreCase(p.getName()) && !p.hasPermission("skychests.clickother")){
                            e.setCancelled(true);
                            p.playSound(p.getLocation(), Sound.BAT_HURT, 1, 1);
                            break;
                        }

                        if(!chest.isDirty())
                            chest.setDirty(true);

                        break;
                    case MAIN:
                        e.setCancelled(true);
                        if (itemStack.isSimilar(GuiItems.getFill()) || itemStack.getType() == Material.AIR) break;

                        id = Utils.getSlot(e.getSlot());

                        if (e.getClick() == ClickType.LEFT) {
                            if (ChestController.hasChestId(dono, id)) {
                                chest = ChestController.get(dono, id);

                                p.openInventory(chest.getInventory());
                                p.playSound(p.getLocation(), Sound.CHEST_OPEN, 1, 1);
                            } else {
                                p.openInventory(ConfirmGui.getBuying(dono, id));
                                p.playSound(p.getLocation(), Sound.CHEST_OPEN, 1, 1);
                            }
                        } else if (e.getClick() == ClickType.RIGHT) {
                            if (ChestController.hasChestId(dono, id)) {
                                IconGui.open(p, dono, id);
                                p.playSound(p.getLocation(), Sound.CHEST_OPEN, 1, 1);
                            } else {
                                p.playSound(p.getLocation(), Sound.BAT_HURT, 1, 1);
                            }
                        } else if (e.getClick() == ClickType.MIDDLE) {
                            if(!p.hasPermission("skychests.delete")) break;
                            if (ChestController.hasChestId(dono, id)) {
                                p.openInventory(ConfirmGui.getDeleting(dono, id));
                                p.playSound(p.getLocation(), Sound.CHEST_OPEN, 1, 1);
                            } else {
                                p.playSound(p.getLocation(), Sound.BAT_HURT, 1, 1);
                            }
                        }
                        break;
                    case ICON:
                        e.setCancelled(true);
                        if (itemStack.isSimilar(GuiItems.getFill())) break;

                        if (itemStack.isSimilar(GuiItems.getBack())) {
                            p.openInventory(MainGui.getPlayerGui(dono, p));
                            p.playSound(p.getLocation(), Sound.CHEST_CLOSE, 1, 1);
                        } else if (itemStack.isSimilar(GuiItems.getPrevious())) {
                            if (IconGui.getPlayerPage(p) == 0) {
                                p.playSound(p.getLocation(), Sound.BAT_HURT, 1, 1);
                                break;
                            }
                            IconGui.previousPage(p, dono, id);
                            p.playSound(p.getLocation(), Sound.CHEST_OPEN, 1, 1);
                        } else if (itemStack.isSimilar(GuiItems.getNext())) {
                            if (IconGui.getPlayerPage(p) == Integer.valueOf(IconGui.getMaxPages() - 1) ) {
                                p.playSound(p.getLocation(), Sound.BAT_HURT, 1, 1);
                                break;
                            }
                            IconGui.nextPage(p, dono, id);
                            p.playSound(p.getLocation(), Sound.CHEST_OPEN, 1, 1);
                        } else {
                            if(itemStack.getType() == Material.AIR){
                                p.playSound(p.getLocation(), Sound.BAT_HURT, 1, 1);
                                break;
                            }
                            chest.setIcon(itemStack);
                            p.closeInventory();
                            p.playSound(p.getLocation(), Sound.CHEST_CLOSE, 1, 1);
                            p.sendMessage(Config.getMsg_IconSet().replaceAll("%icon%", Utils.prettifyText(itemStack.getType().name())).replaceAll("%id%", "" + id));
                        }

                        break;
                    case DELETE:
                        e.setCancelled(true);
                        if (itemStack.isSimilar(GuiItems.getFill())) break;

                        if (itemStack.isSimilar(GuiItems.getBack())) {
                            p.openInventory(MainGui.getPlayerGui(dono, p));
                            p.playSound(p.getLocation(), Sound.CHEST_CLOSE, 1, 1);
                        } else if (itemStack.isSimilar(GuiItems.getConfirm())) {
                            SQL.delete(dono, id, 0);
                            SkyChests.getChestArrayList().remove(chest);
                            p.closeInventory();
                            p.playSound(p.getLocation(), Sound.BLAZE_DEATH, 1, 1);
                        }

                        break;
                    case BUY:
                        e.setCancelled(true);
                        if (itemStack.isSimilar(GuiItems.getFill())) break;

                        if (itemStack.isSimilar(GuiItems.getBack())) {
                            p.openInventory(MainGui.getPlayerGui(dono, p));
                            p.playSound(p.getLocation(), Sound.CHEST_CLOSE, 1, 1);
                        } else if (itemStack.isSimilar(GuiItems.getConfirm())) {
                            if(p.hasPermission("skychests.firstfree") && ChestController.getChestsAmount(dono) == 0){
                                buy(p, dono, id, 0.0);
                                break;
                            }
                            if((ChestController.getChestsAmount(dono) >= Utils.getMaxChests(dono)) && !p.hasPermission("skychests.nomax")){
                                p.sendMessage(Config.getMsg_CantBuy());
                                p.closeInventory();
                                p.playSound(p.getLocation(), Sound.BAT_HURT, 1, 1);
                                break;
                            }
                            if(p.hasPermission("skychests.free")){
                                buy(p, dono, id, 0.0);
                            } else {
                                Double finalPrice = Utils.getFinalPrice(p);

                                if(SkyChests.getEconomy().getBalance(p) < finalPrice){
                                    p.closeInventory();
                                    p.sendMessage(Config.getMsg_NoMoney());
                                    p.playSound(p.getLocation(), Sound.BAT_HURT, 1, 1);
                                } else {
                                    buy(p, dono, id, finalPrice);
                                    SkyChests.getEconomy().withdraw(p, finalPrice);
                                }
                            }
                        }

                        break;
                }
            } else {
                if(((GuiHolder) e.getInventory().getHolder()).getType() == GuiHolder.Type.CHEST) return;
                if(e.getClick() == ClickType.SHIFT_LEFT
                    || e.getClick() == ClickType.SHIFT_RIGHT
                    || e.getClick() == ClickType.DOUBLE_CLICK) e.setCancelled(true);
            }
        }
    }

    private void buy(Player p, String dono, int id, Double price){
        ChestController.create(dono, id);
        p.sendMessage(Config.getMsg_Bought().replaceAll("%id%", "" + id).replaceAll("%custo%", "" + price));
        p.playSound(p.getLocation(), Sound.LEVEL_UP, 1, 1);
        p.closeInventory();
    }

}
