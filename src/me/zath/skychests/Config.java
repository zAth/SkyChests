package me.zath.skychests;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skychests.gui.utils.GuiItems;

import java.util.ArrayList;
import java.util.List;

public class Config {

    private static String cmd_Format, cmd_noConsole, cmd_NoPermission, msg_NpcSet, cmd_SetNpc, cmd_DelNpc, cmd_Reload, msg_NpcDel
        , msg_NpcDelNotSpecified, msg_NpcDelNotNumber, msg_NpcDelNotThatMany, gui_Title, gui_iconTitle, msg_HasNoChests, msg_IconSet
        , gui_ConfirmTitle, gui_ChestTitle, sql_Host, sql_Database,  sql_User, sql_Password, villager_Name, msg_NoMoney, msg_CantBuy, msg_Bought
        , msg_ChestNotSpecified, msg_ChestNotOwned, msg_Reload, cmd_Transmute, msg_Transmuted, msg_TransmuteError, msg_CantBeTransmutedYet
        , msg_PluginNotSpecified;
    private static Double chest_Cost;
    private static Integer sql_Port, chest_SaveDelay, chest_DefaultChestAmount, chest_Chests;
    private static Boolean sql_Enabled, sql_ShowPossibleErros;

    public static void load() {
        msg_PluginNotSpecified = getString("Cmds.PluginNotSpecified");
        msg_CantBeTransmutedYet = getString("Cmds.CantBeTransmutedYet");
        msg_Transmuted = getString("Cmds.Transmuted");
        msg_TransmuteError = getString("Cmds.TransmuteError");
        cmd_Transmute = getString("Cmds.Transmute");
        sql_ShowPossibleErros = getBoolean("SQL.ShowPossibleErros");
        chest_Chests = getInt("Chests");
        msg_ChestNotSpecified = getString("Msg.ChestNotSpecified").replaceAll("&", "§");
        msg_ChestNotOwned = getString("Msg.ChestNotOwned").replaceAll("&", "§");
        chest_Cost = getDouble("Custo");
        msg_Bought = getString("Msg.Bought").replaceAll("&", "§");
        msg_NoMoney = getString("Msg.NoMoney").replaceAll("&", "§").replaceAll("%custo%", "" + chest_Cost);
        msg_CantBuy = getString("Msg.CantBuy").replaceAll("&", "§");
        chest_DefaultChestAmount = getInt("DefaultChestAmount");
        villager_Name = getString("VillagerName").replaceAll("&", "§");
        chest_SaveDelay = getInt("SaveDelay");
        sql_Host = getString("SQL.Host");
        sql_Database = getString("SQL.Database");
        sql_User = getString("SQL.User");
        sql_Password = getString("SQL.Password");
        sql_Port = getInt("SQL.Port");
        sql_Enabled = getBoolean("SQL.Enable");
        msg_IconSet = getString("Msg.IconSet").replaceAll("&", "§");
        msg_HasNoChests = getString("Msg.HasNoChests").replaceAll("&", "§");
        cmd_Format = getString("Cmds.Format").replaceAll("&", "§");
        cmd_noConsole = getString("Cmds.NoConsole").replaceAll("&", "§");
        cmd_NoPermission = getString("Cmds.NoPermission").replaceAll("&", "§");
        msg_NpcSet = getString("Msg.NpcSet").replaceAll("&", "§");
        cmd_SetNpc = getString("Cmds.SetNPC").replaceAll("&", "§");
        cmd_DelNpc = getString("Cmds.DelNPC").replaceAll("&", "§");
        cmd_Reload = getString("Cmds.Reload").replaceAll("&", "§");
        msg_NpcDel = getString("Msg.NpcDel").replaceAll("&", "§");
        msg_Reload = getString("Msg.Reload").replaceAll("&", "§");
        msg_NpcDelNotNumber = getString("Msg.NpcDelNotNumber").replaceAll("&", "§");
        msg_NpcDelNotSpecified = getString("Msg.NpcDelNotSpecified").replaceAll("&", "§");
        msg_NpcDelNotThatMany = getString("Msg.NpcDelNotThatMany").replaceAll("&", "§");
        GuiItems.loadFill(getInt("Items.Fill.Id"), getInt("Items.Fill.Data"));
        GuiItems.loadNotOwned(getString("Items.NotOwned.Name").replaceAll("&", "§"), makeListColory(getList("Items.NotOwned.Lore")), getInt("Items.NotOwned.Id"), getInt("Items.NotOwned.Data"));
        GuiItems.loadOwned(getString("Items.Owned.Name").replaceAll("&", "§"), makeListColory(getList("Items.Owned.Lore")), getInt("Items.Owned.Id"), getInt("Items.Owned.Data"));
        GuiItems.loadBack(getString("Items.Back.Name").replaceAll("&", "§"), makeListColory(getList("Items.Back.Lore")), getInt("Items.Back.Id"), getInt("Items.Back.Data"));
        GuiItems.loadNext(getString("Items.Next.Name").replaceAll("&", "§"), makeListColory(getList("Items.Next.Lore")), getInt("Items.Next.Id"), getInt("Items.Next.Data"));
        GuiItems.loadPrevious(getString("Items.Previous.Name").replaceAll("&", "§"), makeListColory(getList("Items.Previous.Lore")), getInt("Items.Previous.Id"), getInt("Items.Previous.Data"));
        GuiItems.loadConfirm(getString("Items.Confirm.Name").replaceAll("&", "§"), makeListColory(getList("Items.Confirm.Lore")), getInt("Items.Confirm.Id"), getInt("Items.Confirm.Data"));
        gui_Title = getString("GuiTitle").replaceAll("&", "§");
        gui_iconTitle = getString("IconTitle").replaceAll("&", "§");
        gui_ConfirmTitle = getString("ConfirmTitle").replaceAll("&", "§");
        gui_ChestTitle = getString("ChestTitle").replaceAll("&", "§");
    }

    public static String getMsg_Transmuted() {
        return msg_Transmuted;
    }

    public static String getMsg_TransmuteError() {
        return msg_TransmuteError;
    }

    public static String getMsg_CantBeTransmutedYet() {
        return msg_CantBeTransmutedYet;
    }

    public static String getMsg_PluginNotSpecified() {
        return msg_PluginNotSpecified;
    }

    public static String getCmd_Transmute() {
        return cmd_Transmute;
    }

    public static String getMsg_Reload() {
        return msg_Reload;
    }

    public static Boolean getSql_ShowPossibleErros() {
        return sql_ShowPossibleErros;
    }

    public static Integer getChest_Chests() {
        return chest_Chests;
    }

    public static String getMsg_ChestNotSpecified() {
        return msg_ChestNotSpecified;
    }

    public static String getMsg_ChestNotOwned() {
        return msg_ChestNotOwned;
    }

    public static String getMsg_Bought() {
        return msg_Bought;
    }

    public static String getMsg_NoMoney() {
        return msg_NoMoney;
    }

    public static String getMsg_CantBuy() {
        return msg_CantBuy;
    }

    public static Integer getChest_DefaultChestAmount() {
        return chest_DefaultChestAmount;
    }

    public static String getVillager_Name() {
        return villager_Name;
    }

    public static Integer getChest_SaveDelay() {
        return chest_SaveDelay;
    }

    public static String getSql_Host() {
        return sql_Host;
    }

    public static String getSql_Database() {
        return sql_Database;
    }

    public static String getSql_User() {
        return sql_User;
    }

    public static String getSql_Password() {
        return sql_Password;
    }

    public static Integer getSql_Port() {
        return sql_Port;
    }

    public static Boolean getSql_Enabled() {
        return sql_Enabled;
    }

    public static Double getChest_Cost() {
        return chest_Cost;
    }// below

    public static String getGui_ChestTitle() {
        return gui_ChestTitle;
    }

    public static String getGui_ConfirmTitle() {
        return gui_ConfirmTitle;
    }

    public static String getGui_IconTitle() {
        return gui_iconTitle;
    }

    public static String getGui_Title() {
        return gui_Title;
    }

    public static String getMsg_IconSet() {
        return msg_IconSet;
    }

    public static String getMsg_HasNoChests() {
        return msg_HasNoChests;
    }

    public static String getMsg_NpcDelNotThatMany() {
        return msg_NpcDelNotThatMany;
    }

    public static String getMsg_NpcDelNotNumber() {
        return msg_NpcDelNotNumber;
    }

    public static String getMsg_NpcDelNotSpecified() {
        return msg_NpcDelNotSpecified;
    }

    public static String getMsg_NpcDel() {
        return msg_NpcDel;
    }

    public static String getCmd_DelNpc() {
        return cmd_DelNpc;
    }

    public static String getCmd_SetNpc() {
        return cmd_SetNpc;
    }

    public static String getCmd_Reload() {
        return cmd_Reload;
    }

    public static String getMsg_NpcSet() {
        return msg_NpcSet;
    }

    public static String getCmd_Format() {
        return cmd_Format;
    }

    public static String getCmd_NoConsole() {
        return cmd_noConsole;
    }

    public static String getCmd_NoPermission() {
        return cmd_NoPermission;
    }

    private static Boolean getBoolean(String dir) {
        Boolean toReturn;
        try {
            toReturn = SkyChests.getSkychests().getConfig().getBoolean(dir);
        } catch (Exception e) {
            SkyChests.getSkychests().getServer().getPluginManager().disablePlugin(SkyChests.getSkychests());
            throw new IllegalArgumentException("Invalid config: " + e.getMessage() + "\n");
        }
        return toReturn;
    }

    private static String getString(String dir) {
        String toReturn;
        try {
            toReturn = SkyChests.getSkychests().getConfig().getString(dir);
        } catch (Exception e) {
            SkyChests.getSkychests().getServer().getPluginManager().disablePlugin(SkyChests.getSkychests());
            throw new IllegalArgumentException("Invalid config: " + e.getMessage() + "\n");
        }
        return toReturn;
    }

    private static int getInt(String dir) {
        int toReturn;
        try {
            toReturn = SkyChests.getSkychests().getConfig().getInt(dir);
        } catch (Exception e) {
            SkyChests.getSkychests().getServer().getPluginManager().disablePlugin(SkyChests.getSkychests());
            throw new IllegalArgumentException("Invalid config: " + e.getMessage() + "\n");
        }
        return toReturn;
    }

    private static double getDouble(String dir) {
        Double toReturn;
        try {
            toReturn = SkyChests.getSkychests().getConfig().getDouble(dir);
        } catch (Exception e) {
            SkyChests.getSkychests().getServer().getPluginManager().disablePlugin(SkyChests.getSkychests());
            throw new IllegalArgumentException("Invalid config: " + e.getMessage() + "\n");
        }
        return toReturn;
    }

    private static List<String> getList(String dir) {
        List<String> toReturn;
        try {
            toReturn = SkyChests.getSkychests().getConfig().getStringList(dir);
        } catch (Exception e) {
            SkyChests.getSkychests().getServer().getPluginManager().disablePlugin(SkyChests.getSkychests());
            throw new IllegalArgumentException("Invalid config: " + e.getMessage() + "\n");
        }
        return toReturn;
    }

    private static List<String> makeListColory(List<String> l) {
        List<String> list = new ArrayList<>();
        for (String str : l) {
            list.add(str.replaceAll("&", "§"));
        }
        return list;
    }

}
