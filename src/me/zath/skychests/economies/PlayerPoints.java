package me.zath.skychests.economies;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skychests.SkyChests;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class PlayerPoints implements IEconomy {

    private org.black_ixx.playerpoints.PlayerPointsAPI playerPointsAPI;

    public PlayerPoints(SkyChests skyChests) {
        Plugin plugin = skyChests.getServer().getPluginManager().getPlugin("PlayerPoints");
        playerPointsAPI = org.black_ixx.playerpoints.PlayerPoints.class.cast(plugin).getAPI();
    }

    @Override
    public void withdraw(Player player, double amount) {
        playerPointsAPI.take(player.getName(), (int) Math.round(amount));
    }

    @Override
    public double getBalance(Player player) {
        return playerPointsAPI.look(player.getName());
    }

}
