package me.zath.skychests.economies;
/*
 * MC 
 * Created by zAth
 */

import com.minepag.cash.Cash;
import org.bukkit.entity.Player;

public class MinePag implements IEconomy {

    @Override
    public void withdraw(Player player, double amount) {
        Cash.setCash(player.getName(), getBalance(player) - amount);
    }

    @Override
    public double getBalance(Player player) {
        return Cash.getCash(player.getName());
    }

}
