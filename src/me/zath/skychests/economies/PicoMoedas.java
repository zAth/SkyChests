package me.zath.skychests.economies;
/*
 * MC 
 * Created by zAth
 */

import com.mrpowergamerbr.picomoedas.PicoMoedasAPI;
import org.bukkit.entity.Player;

public class PicoMoedas implements IEconomy {

    @Override
    public void withdraw(Player player, double amount) {
        PicoMoedasAPI.editBalance(player, getBalance(player) - amount);
    }

    @Override
    public double getBalance(Player player) {
        return PicoMoedasAPI.getBalance(player).getValue();
    }

}
