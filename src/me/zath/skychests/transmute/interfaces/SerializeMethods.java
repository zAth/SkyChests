package me.zath.skychests.transmute.interfaces;
/*
 * MC 
 * Created by zAth
 */

import org.bukkit.inventory.ItemStack;

public interface SerializeMethods {

    ItemStack[] toItemStackList(String string);
    ItemStack toItemStack(String string);

}
