package me.zath.skychests.transmute.interfaces;
/*
 * MC 
 * Created by zAth
 */

public class SqlTable {

    private String tableName;
    private String ownerColumn;
    private String idColumn;
    private String serializedIconColumn;
    private String serializedContentsColumn;

    public SqlTable(String tableName, String ownerColumn, String idColumn, String serializedIconColumn, String serializedContentsColumn) {
        this.tableName = tableName;
        this.ownerColumn = ownerColumn;
        this.idColumn = idColumn;
        this.serializedIconColumn = serializedIconColumn;
        this.serializedContentsColumn = serializedContentsColumn;
    }

    public String getTableName() {
        return tableName;
    }

    public String getOwnerColumn() {
        return ownerColumn;
    }

    public String getIdColumn() {
        return idColumn;
    }

    public String getSerializedIconColumn() {
        return serializedIconColumn;
    }

    public String getSerializedContentsColumn() {
        return serializedContentsColumn;
    }

    @Override
    public String toString() {
        return "SqlTable{" +
            "tableName='" + tableName + '\'' +
            ", ownerColumn='" + ownerColumn + '\'' +
            ", idColumn='" + idColumn + '\'' +
            ", serializedIconColumn='" + serializedIconColumn + '\'' +
            ", serializedContentsColumn='" + serializedContentsColumn + '\'' +
            '}';
    }
}
