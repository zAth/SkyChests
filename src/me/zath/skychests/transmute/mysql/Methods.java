package me.zath.skychests.transmute.mysql;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skychests.transmute.interfaces.SerializeMethods;
import me.zath.skychests.utils.Serialize;
import org.bukkit.inventory.ItemStack;

public class Methods implements SerializeMethods {

    @Override
    public ItemStack[] toItemStackList(String string) {
        return Serialize.fromBase64List(string);
    }

    @Override
    public ItemStack toItemStack(String string) {
        return Serialize.simpleDeserialize(string);
    }

}
