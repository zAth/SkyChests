package me.zath.skychests.transmute.mysql;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skychests.Config;
import me.zath.skychests.transmute.interfaces.SerializeMethods;
import me.zath.skychests.transmute.interfaces.SqlProperties;
import me.zath.skychests.transmute.interfaces.SqlTable;
import me.zath.skychests.transmute.interfaces.Transmutable;

public class MySql implements Transmutable {

    @Override
    public String getPluginName() {
        return "SkyChests";
    }

    @Override
    public boolean isMySql() {
        return true;
    }

    @Override
    public SqlProperties getSqlProperties() {
        String sqlHost = Config.getSql_Host();
        int sqlPort = Config.getSql_Port();
        String sqlDatabase = Config.getSql_Database();
        String sqlUser = Config.getSql_User();
        String sqlPassword = Config.getSql_Password();

        return new SqlProperties(sqlUser, sqlHost, sqlPort, sqlDatabase, sqlPassword, "chests.db");
    }

    @Override
    public SqlTable getSqlTable() {
        return new SqlTable("chests", "owner", "id", "serializedicon", "serializedcontents");
    }

    @Override
    public SerializeMethods getSerializeMethods() {
        return new Methods();
    }
}
