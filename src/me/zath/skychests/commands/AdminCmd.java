package me.zath.skychests.commands;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skychests.Config;
import me.zath.skychests.SkyChests;
import me.zath.skychests.commands.sub.DelNPC;
import me.zath.skychests.commands.sub.Reload;
import me.zath.skychests.commands.sub.SetNPC;
import me.zath.skychests.commands.sub.Transmute;
import me.zath.skychests.controllers.ChestController;
import me.zath.skychests.gui.MainGui;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

public class AdminCmd implements CommandExecutor{

    private static final Map<String, ICommand> cmds = new HashMap<>();
    private static String format,noConsole,noPermission;

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(cmd.getName().equalsIgnoreCase("skychests")) {
            if (args.length == 0) {
                for (String entry : cmds.keySet()) {
                    sender.sendMessage(format.replaceAll("%cmd%", entry).replaceAll("%descricao%", cmds.get(entry).getDescription()).replaceAll("&", "§"));
                }
            } else {
                ICommand icmd = cmds.get(args[0].toLowerCase());
                if (icmd != null) {
                    if (!icmd.supportsConsole() && (sender instanceof ConsoleCommandSender)) {
                        sender.sendMessage(noConsole);
                        return true;
                    }
                    if (!sender.hasPermission(icmd.getPermission())) {
                        sender.sendMessage(noPermission);
                        return true;
                    }
                    icmd.execute(sender, args, SkyChests.getSkychests());
                    return true;
                } else {
                    if (!sender.hasPermission("skychests.open.other")) {
                        sender.sendMessage(noPermission);
                        return true;
                    }
                    if(!ChestController.hasChests(args[0].toLowerCase())){
                        sender.sendMessage(Config.getMsg_HasNoChests());
                        return true;
                    }
                    ((Player)sender).openInventory(MainGui.getPlayerGui(args[0].toLowerCase(), (Player) sender));
                    ((Player)sender).playSound(((Player)sender).getLocation(), Sound.CHEST_OPEN, 1, 1);
                }
            }
        }
        return false;
    }

    public static void registerCommands(){
        format = Config.getCmd_Format();
        noConsole = Config.getCmd_NoConsole();
        noPermission = Config.getCmd_NoPermission();

        cmds.put("setnpc", new SetNPC());
        SetNPC.load(Config.getCmd_SetNpc());
        cmds.put("delnpc", new DelNPC());
        DelNPC.load(Config.getCmd_DelNpc());
        cmds.put("reload", new Reload());
        Reload.load(Config.getCmd_Reload());
        cmds.put("converter", new Transmute());
        Transmute.load(Config.getCmd_Transmute());
    }

}
