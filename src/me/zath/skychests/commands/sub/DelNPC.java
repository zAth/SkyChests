package me.zath.skychests.commands.sub;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skychests.Config;
import me.zath.skychests.SkyChests;
import me.zath.skychests.utils.Utils;
import me.zath.skychests.commands.ICommand;
import me.zath.skychests.controllers.VillagerController;
import me.zath.skychests.objects.Villager;
import org.bukkit.command.CommandSender;

public class DelNPC implements ICommand {

    private static String desc;

    @Override
    public void execute(CommandSender sender, String[] args, SkyChests plugin) {
        if(args.length != 2){
            sender.sendMessage(Config.getMsg_NpcDelNotSpecified());
            return;
        }
        if(!Utils.isInt(args[1])){
            sender.sendMessage(Config.getMsg_NpcDelNotNumber());
            return;
        }
        if(Integer.parseInt(args[1]) >= SkyChests.getVillagerArrayList().size()){
            sender.sendMessage(Config.getMsg_NpcDelNotThatMany());
            return;
        }
        Villager villager = VillagerController.get(Integer.parseInt(args[1]));
        villager.kill();

        villager.getHologram().hideAll();
        villager.getHologram().delete();

        villager.delete();
        VillagerController.remove(Integer.parseInt(args[1]));
        sender.sendMessage(Config.getMsg_NpcDel().replaceAll("%npc%", "" + Integer.parseInt(args[1])));
    }

    public static void load(String string){
        desc = string;
    }

    @Override
    public boolean supportsConsole() {
        return false;
    }

    @Override
    public String getPermission() {
        return "skychests.delnpc";
    }

    @Override
    public String getDescription() {
        return desc;
    }
}
